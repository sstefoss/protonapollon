from os import listdir, rename
from shutil import copy, move
from PIL import Image
from bs4 import BeautifulSoup as Soup

width = 320
fulls_folder = "./images/fulls/"
thumbs_folder = "./images/thumbs/"
new_images_folder = "./new/"
html_index = "./index.html"

def resize(filename):
    img = Image.open(filename)
    wpercent = (width / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.convert("RGB")
    img = img.resize((width, hsize), Image.ANTIALIAS)
    img.save(filename)

def updated_filename(index):
    return code + '-' + str(index) + '.jpg'

# full_location = input("When where these pictures taken? eg Paris, France ")
code = input("Suggested filename? eg paris ")
images = listdir(new_images_folder)

for index, image in enumerate(images):
    if image == ".DS_Store" or image == "":
        continue
    
    print("renaming new images according to code + index + jpg format ...")
    rename(new_images_folder + image, new_images_folder + updated_filename(index))

    print("copying files to fulls folder...")
    copy(new_images_folder + updated_filename(index), fulls_folder + updated_filename(index))

    print("creating thumb...")
    resize(new_images_folder + updated_filename(index))

    print("moving thumb to thumbs folder...")
    move(new_images_folder + updated_filename(index), thumbs_folder + updated_filename(index))

# print("injecting html code...")
# with open(html_index, 'r') as html_file:
#     html = html_file.read().replace('\n', '')

# soup = Soup(html)
# title = soup.find('title')
# meta = soup.new_tag('meta')
# meta['sather'] = "mather"
# title.insert_after(meta)

# new_index = open("new_index.html", "w")
# new_index.write(str(soup))
# new_index.close()
    
    
